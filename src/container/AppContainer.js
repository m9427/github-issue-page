import React, { Component } from 'react'
import '../css/AppContainer.css'
import TitleContainer from './TitleContainer'
import ListContainer from './ListContainer'
import BottomPageNav from './BottomPageNav'

import dataArray1 from '../data/issueData.json'
import HeadContainer from './HeadContainer'
import DetailsDisplay from './DetailsDisplay'

export class AppContainer1 extends Component {
  constructor(props) {
    super(props)

    this.state = {
      btnClicked: 0,
      dataArray2: [],
      ownerName: 'highlightjs',
      repoName: 'highlight.js',
      dataToSendToNextContainer: [],
      issueNum: 0,
      onHomePage: true
    }
    // this.getIssueData()
    this.formatData = this.formatData.bind(this);

  }
  // debounce=(fun,delay)=>{
  //   clearTimeout(this.debounceTimer);
  //   this.debounceTimer= setTimeout(fun,delay);
  // }
  // logFun=()=>{
  //   console.log("This is a log function");
  // }

  componentDidMount() {
    console.log("data request function call started in lifecycle")
    this.getIssueData(this.state.ownerName, this.state.repoName)
  }


  getIssueData = (ownerName, repoName) => {
    console.log("data requested")
    fetch(`https://api.github.com/repos/${ownerName}/${repoName}/issues?per_page=100&state=all`).then((data) => data.json()).then((gotData) => {
      console.log("data sent")
      console.log(gotData)
      this.setState({ dataArray2: gotData})
    }).catch(console.log)

  }

  getOpenAndClosedIssues = (dataArray) => {

    return dataArray.reduce((acc, curr) => {
      if (curr.action === "open") {
        acc[0] += 1;
      }
      else if (curr.action === "closed") {
        acc[1] += 1;
      }
      return acc;
    }, [0, 0])
  }
  nextList = (btnNum) => {
    // console.log(btnNum);
    if (btnNum === 'Prev') {
      // console.log("in");
      this.setState((state)=>({ btnClicked: state.btnClicked - 1 }))
    }
    else if (btnNum === 'Next') {
      (this.state.btnClicked < 9) ? this.setState((state)=>({ btnClicked: state.btnClicked + 1 })) : console.log("already reached max");
    }
    else {
      this.setState((state)=>({ btnClicked: btnNum - 1 }))
    }
    // console.log(this.state.btnClicked);
  }

  selectedDataToDisplay = (dataArray) => {
    let startIndex = this.state.btnClicked * 10;
    return dataArray.filter((dataObj, index) => (index >= startIndex && index < startIndex + 10))

  }

  formatData = (myArray) => {
    // console.log(Array.isArray(myArray))
    return myArray?.reduce((acc, curr) => {
      // console.log('abcd',curr.labels[0] ||"none");
      let abcd = curr.labels[0];
      // console.log(abcd?.name ? abcd.name : 'none')
      acc.push({
        id: curr.number,
        title: curr.title,
        comment: `Added by ${curr.user.login}`,
        action: curr.state,
        labelName: abcd?.name ? abcd.name : 'none',
        commentsNum: curr.comments,
      })
      return acc;
    }, [])
  }

  getIssueDetailsbyNum=(issueNum)=> {
    console.log("data requested by click")
    // console.log(`https://api.github.com/repos/${this.state.ownerName}/${this.state.repoName}/issues/${parseInt(issueNum)}`)
    fetch(`https://api.github.com/repos/${this.state.ownerName}/${this.state.repoName}/issues/${parseInt(issueNum)}/comments`).then((data) => data.json()).then((gotData) => {
      console.log("data sent")
      console.log(gotData);
      this.setState({ dataToSendToNextContainer: gotData, onHomePage: true });
    }).catch(console.log)
  }

  moveToHomePage() {
    this.setState({ onHomePage: true })
  }

  render() {
    console.log("rendered")
    let dataArray = this.formatData(this.state.dataArray2) ?? dataArray1;
    console.log("formatted data", dataArray);
    let openCloseIssues = this.getOpenAndClosedIssues(dataArray);
    // console.log(openCloseIssues);
    let dataArrayToSend = this.selectedDataToDisplay(dataArray);
    // let nextList = [prev, one, two, three, prev, one, two, three, prev, one, two];
    return (
      this.state.onHomePage ?
        <div>
          <HeadContainer repoName={this.state.repoName} />
          <div className='top-container'>
            <TitleContainer openCloseIssues={openCloseIssues} />
            <ListContainer dataArray={dataArrayToSend} getListIssueDetails={this.getIssueDetailsbyNum} />
            <BottomPageNav nextList={this.nextList} />
          </div>
        </div>
        :
        <div>
          <HeadContainer repoName={this.state.repoName} />
          <DetailsDisplay sentData={this.state.dataToSendToNextContainer} backButtonFunc={this.moveToHomePage} />
        </div>

    )
  }
}


export default AppContainer1