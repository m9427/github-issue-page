import React from 'react'
import '../css/ListContainer.css'
import List from './List'




function ListContainer({ dataArray, getListIssueDetails }) {
    let listToDisplay = dataArray.map((data) => {
        return <List key={data.id} data={data} listIssueDetailFun={getListIssueDetails} />
    })
    return (
        <div className='list-cont'>
            {listToDisplay}
        </div>

    )
}

export default ListContainer