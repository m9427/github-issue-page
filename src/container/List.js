import React from 'react'
import "../css/List.css"

function List({data, listIssueDetailFun}) {
  return (
    <div className='list-tile'>
        <div className='cont1'>
            <div className='list-button' onClick={()=>{listIssueDetailFun(data.id)}}></div>
        </div>
        <div className='cont2'>
            <div className='issue-title'> {data.title}</div>
            <div className='issue-comment'>#{data.id} {data.comment}</div>
        </div>
        <div className='cont3'>
            <div className='info-box1'>{data.action}</div>
            <div className='info-box2'>{data.commentsNum}</div>
            <div className='info-box3'>{data.labelName}</div>
        </div>
    </div>
  )
}

export default List