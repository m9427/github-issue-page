import React from 'react'
import '../css/TitleContainer.css'
import TitleContainerTiles from './TitleContainerTiles'

function TitleContainer({openCloseIssues}) {
  return (
    <div className='title-cont'>
    <TitleContainerTiles openCloseIssues={openCloseIssues} />
    </div>
  )
}

export default TitleContainer