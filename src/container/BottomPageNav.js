import React from 'react'
import '../css/BottomPageNav.css'
import BottomNavTile from './BottomNavTile';

let navBars = ['Prev', 1, 2, 3, 4, 5, 6, 7, 8, 9, 'Next']

function BottomPageNav({ nextList }) {
    let barsAssigned = navBars.map((num, index) => {
        return <BottomNavTile key={index} numAssign={num} nextList={nextList} />
    });
    return (
        <div className="bottom-nav">
            {barsAssigned}
        </div>
    )
}

export default BottomPageNav