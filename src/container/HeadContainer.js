import React from 'react'
import '../css/HeadContainer.css'

function HeadContainer({repoName}) {
  return (
    <div className='my-header'>Issues for :{repoName}</div>
  )
}

export default HeadContainer