import React from 'react'
import '../css/BottomNavTile.css'

function BottomNavTile({numAssign,nextList}) {
  return (
    <div className='btn' onClick={()=>nextList(numAssign)}>{numAssign}</div>
  )
}

export default BottomNavTile