import React from 'react'

function DetailsDisplay() {
  return (
    <div className='main-container'>
        <div className='back-container'>
            <div className='back-btn'> Back </div>
        </div>
        <div className='head-container'>
            <div className='head-text'></div>
            <div className='head-subheading'>
                <div className='status'></div>
                <div className='time'></div>
                <div className='user'></div>
            </div>
        </div>
    </div>
  )
}

export default DetailsDisplay