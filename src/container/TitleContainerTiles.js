import React from 'react'
import '../css/TitleContainerTiles.css'

function TitleContainerTiles({openCloseIssues} ) {
  return (
    <div className='left-title-cont'>
        <div className='top-count-cont'>
        <div className='num-count'>{openCloseIssues[0]}</div>
        <div className='indicator'>Open</div>
        </div>
        <div className='top-count-cont'>
        <div className='num-count'>{openCloseIssues[1]}</div>
        <div className='indicator'>Closed</div>
        </div>
    </div>
  )
}

export default TitleContainerTiles